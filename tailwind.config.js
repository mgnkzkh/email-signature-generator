module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1440px',
      // => @media (min-width: 1536px) { ... }
    },
    colors: {
      wattle: {
        light: '#e5e9a2',
        DEFAULT: '#d6db51',
      },
      charcoal: {
        DEFAULT: '#2d4053',
      },
      atlantis: {
        DEFAULT: '#a6bd32'
      },
      gray: {
        DEFAULT: '#5c6670',
        light: '#abb3ba'
      },
      dark: {
        DEFAULT: '#2b2b2b',
      },
      light: {
        DEFAULT: '#f8fafc'
      },
      white: {
        DEFAULT: '#fff',
      },
      red: {
        DEFAULT: '#f56c6c',
      },
    },
    fontSize: {
      'sm': '.75rem',
      'base': '1rem',
      'lg': '1.5rem',
      'xl': '1.625rem',
      '2xl': '2.5rem',
      '3xl': '3.875rem',
     },
    fontFamily: {
     'primary': 'DM Sans',
    },
    letterSpacing: {
     tight: '-0.03em',
     normal: '0',
     wide: '0.03em',
    },
    borderRadius: {
     DEFAULT: '4px',
    },
    container: {
     center: true,
    },
  },
  variants: {
    extend: {
      spacing: {
        '26': '6.625rem',
      }
    },
  },
  plugins: [],
}
